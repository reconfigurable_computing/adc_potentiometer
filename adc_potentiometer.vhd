library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity adc_potentiometer is
	port 
	(
		SW : in std_logic_vector (9 downto 0);
		ADC_CLK_10 : in std_logic;
		HEX0 : out std_logic_vector (7 downto 0);
		HEX1 : out std_logic_vector (7 downto 0);
		HEX2 : out std_logic_vector (7 downto 0)
	);
end entity adc_potentiometer;

architecture Behavioral of adc_potentiometer is

signal clk, locked, com_valid, com_star, com_stop, com_ready, 
		resp_valid, resp_star, resp_stop : std_logic;
		
signal resp_data : std_logic_vector (11 downto 0);
		
signal com_channel, resp_channel : std_logic_vector (4 downto 0);

component adc_interface
	port (
		adc_pll_clock_clk      : in  std_logic                     := '0';             --  adc_pll_clock.clk
		adc_pll_locked_export  : in  std_logic                     := '0';             -- adc_pll_locked.export
		clock_clk              : in  std_logic                     := '0';             --          clock.clk
		command_valid          : in  std_logic                     := '0';             --        command.valid
		command_channel        : in  std_logic_vector(4 downto 0)  := (others => '0'); --               .channel
		command_startofpacket  : in  std_logic                     := '0';             --               .startofpacket
		command_endofpacket    : in  std_logic                     := '0';             --               .endofpacket
		command_ready          : out std_logic;                                        --               .ready
		reset_sink_reset_n     : in  std_logic                     := '0';             --     reset_sink.reset_n
		response_valid         : out std_logic;                                        --       response.valid
		response_channel       : out std_logic_vector(4 downto 0);                     --               .channel
		response_data          : out std_logic_vector(11 downto 0);                    --               .data
		response_startofpacket : out std_logic;                                        --               .startofpacket
		response_endofpacket   : out std_logic                                         --               .endofpacket
	);
end component;

component pll
	port
	(
		inclk0		: IN STD_LOGIC  := '0';
		c0		: OUT STD_LOGIC ;
		locked		: OUT STD_LOGIC 
	);
end component;

component readfsm
	port 
	(
		CLK 						  : in std_logic;
		RST 						  : in std_logic;
		command_ready          : in std_logic;                                          --               .ready
		response_valid         : in std_logic;                                          --       response.valid
		response_channel       : in std_logic_vector(4 downto 0);                       --               .channel
		response_data          : in std_logic_vector(11 downto 0);                      --               .data
		response_startofpacket : in std_logic;                                          --               .startofpacket
		response_endofpacket   : in std_logic;                                          --               .endofpacket
		
		command_valid          : out  std_logic                     := '0';             --        command.valid
		command_channel        : out  std_logic_vector(4 downto 0)  := (others => '0'); --               .channel
		command_startofpacket  : out  std_logic                     := '0';             --               .startofpacket
		command_endofpacket    : out  std_logic                     := '0';              --               .endofpacket
		HEX0						  : out 	std_logic_vector(7 downto 0);
		HEX1						  : out 	std_logic_vector(7 downto 0);
		HEX2						  : out 	std_logic_vector(7 downto 0)
	);
end component;



begin

my_adc : adc_interface
	port map
	(
		adc_pll_clock_clk => clk,  				-- adc_pll_clock.clk
		adc_pll_locked_export => locked, 		-- adc_pll_locked.export
		clock_clk => clk, 							-- clock.clk
		command_valid => com_valid,				-- command.valid
		command_channel => com_channel,			-- .channel
		command_startofpacket => com_star,		-- .startofpacket
		command_endofpacket => com_stop,			-- .endofpacket
		command_ready => com_ready,
		reset_sink_reset_n => SW(0),					-- reset_sink.reset_n
		response_valid => resp_valid,				-- response.valid
		response_channel => resp_channel,		-- .channel
		response_data => resp_data, 				-- .data
		response_startofpacket => resp_star, 	-- .startofpacket
		response_endofpacket => resp_stop		-- .endofpacket
	);

my_pll : pll
	port map
	(
		inclk0 => ADC_CLK_10,
		c0	=> clk,
		locked => locked
	);
	
my_readfsm : readfsm
	port map
	(
		CLK => clk,
		RST => SW(0),
		command_ready => com_ready,       
		response_valid => resp_valid,
		response_channel => resp_channel,
		response_data => resp_data,
		response_startofpacket => resp_star,
		response_endofpacket => resp_stop,
		
		command_valid => com_valid,
		command_channel => com_channel,
		command_startofpacket => com_star,
		command_endofpacket => com_stop,
		HEX0 => HEX0,
		HEX1 => HEX1,
		HEX2 => HEX2			
	);
	
	


end architecture;