library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity readfsm is
	port 
	(
		CLK 						  : in std_logic;
		RST 						  : in std_logic;
		command_ready          : in std_logic;                                          --               .ready
		response_valid         : in std_logic;                                          --       response.valid
		response_channel       : in std_logic_vector(4 downto 0);                       --               .channel
		response_data          : in std_logic_vector(11 downto 0);                      --               .data
		response_startofpacket : in std_logic;                                          --               .startofpacket
		response_endofpacket   : in std_logic;                                          --               .endofpacket
		
		command_valid          : out  std_logic                     := '0';             --        command.valid
		command_channel        : out  std_logic_vector(4 downto 0)  := (others => '0'); --               .channel
		command_startofpacket  : out  std_logic                     := '0';             --               .startofpacket
		command_endofpacket    : out  std_logic                     := '0';              --               .endofpacket
		HEX0						  : out 	std_logic_vector(7 downto 0) := X"C0";
		HEX1						  : out 	std_logic_vector(7 downto 0) := X"C0";
		HEX2						  : out 	std_logic_vector(7 downto 0) := X"C0"
	);
end entity readfsm;

architecture Behavioral of readfsm is
type seg_table is array (0 to 15) of std_logic_vector(7 downto 0);

constant lut : seg_table := (X"C0", X"F9", X"A4", X"B0", 
										 X"99", X"92", X"82", X"F8", 
										 X"80", X"98", X"88", X"83", 
										 X"C6", X"A1", X"86", X"8E");
										 
	type state_type is (idle, request_data, wait_for_data, counting, acquire_data);
	signal cstate, nstate : state_type;
	
	signal sum, nsum : unsigned (2 downto 0);
	signal count, ncount : natural;
	signal out_dat, nout_dat : std_logic_vector (11 downto 0);

begin
	process(CLK)
	begin
		if rising_edge(CLK) then
			if RST = '0' then
				cstate <= idle;
				count <= 0;
			else
				cstate <= nstate;
				count <= ncount;
				out_dat <= nout_dat;
				HEX0 <= lut(to_integer(unsigned(out_dat(3 downto 0))));
				HEX1 <= lut(to_integer(unsigned(out_dat(7 downto 4)))); 
				HEX2 <= lut(to_integer(unsigned(out_dat(11 downto 8))));
			end if;
		end if;
	end process;
	
	process(cstate, sum, count)
	begin
		case cstate is
			when idle =>
				nout_dat <= out_dat;
				command_valid <= '0';
				command_channel <= "00001";--(others => '0');
				ncount <= count + 1;
				if count = 10000000 then
					nstate <= request_data;
				else 
					nstate <= idle;
				end if;
				
			when request_data =>
				nout_dat <= out_dat;
				ncount <= 0;
				command_valid <= '1';
				command_channel <= "00001";--(others => '0');
				nstate <= wait_for_data;
			
				
			when wait_for_data =>
				nout_dat <= out_dat;
				command_valid <= '0';
				ncount <= 0;
				command_channel <= "00001";--(others => '0');
				if command_ready = '1' then
					nstate <= counting;
				else 
					nstate <= wait_for_data;
				end if;
			
			when counting =>
				nout_dat <= out_dat;
				command_valid <= '0';
				command_channel <= "00001";
				ncount <= count + 1;
				if count = 8 then
					nstate <= acquire_data;
				else 
					nstate <= counting;
				end if;
				
			when acquire_data =>
				ncount <= 0;
				command_valid <= '0';
				command_channel <= "00001";--(others => '0');
				nout_dat <= response_data;
				nstate <= idle;
			
			when others =>
				nout_dat <= out_dat;
				ncount <= 0;
				command_valid <= '0';
				command_channel <= "00001";--(others => '0');
		end case;
	end process;

end architecture;